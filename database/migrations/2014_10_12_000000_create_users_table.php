<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
                $table->increments('id');
                $table->string('name');
                $table->string('username')->unique();
                $table->string('email')->unique();
                $table->string('password');
                $table->string('level')->default('user')->nullable();
                $table->integer('no_pegawai')->unique()->nullable();
                $table->string('jabatan')->nullable();
                $table->enum('jk_pegawai', ['Laki-laki', 'Perempuan'])->nullable();
                $table->date('tgl_lahir')->nullable();
                $table->string('status_pegawai')->nullable();
                $table->string('agama')->nullable();
                $table->string('tempat_lahir')->nullable();
                $table->string('alamat')->nullable();
                $table->string('no_hp')->unique()->nullable();
                $table->rememberToken();
                $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
