<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Tugas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tugas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('no_pegawai')->index();
            $table->string('nama_pegawai')->index();
            $table->string('JenisTugas')->index();
            $table->date('DeadlineTugas')->index();
            $table->date('TanggalSelesai')->nullable();
            $table->string('keterangan')->index();
           
            $table->unsignedInteger('pegawai_id');
            $table->unsignedInteger('admin_id')->nullable();

            $table->foreign('pegawai_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('admin_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
