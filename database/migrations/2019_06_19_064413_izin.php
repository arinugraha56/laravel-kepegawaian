<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Izin extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('izin', function (Blueprint $table) {
            $table->increments('id');
            $table->date('tgl_mulai_izin')->index();
            $table->date('tgl_selesai_izin')->index();
            $table->string('status_izin')->index();
            $table->string('alasan_pegawai')->index();

            $table->unsignedInteger('pegawai_id');
            $table->unsignedInteger('admin_id')->nullable();

            $table->foreign('pegawai_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('admin_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
