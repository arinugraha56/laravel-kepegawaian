<?php
use App\pegawai;
use Illuminate\Support\Facades\Input;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('auth/login', function () {
    return view('auth/login');
});

Route::get('auth/register', function () {
    return view('auth/register');
});

Auth::routes();
    Route::get('tugas/tambah', 'TugasController@create');
    Route::post('tugas/store', 'TugasController@store');
    Route::get('tugas/detail/{id}', 'TugasController@detail');
    Route::put('tugas/selesai/{id}', 'TugasController@update');
    Route::get('tugas/proses-tugas/{id}', 'TugasController@proses');

    // Route::get('pegawai/create', 'PegawaiController@create');
    // Route::post('pegawai/store', 'PegawaiController@store');
    Route::get('pegawai/show/{id}', 'PegawaiController@show');
    Route::get('pegawai/edit/{id}','PegawaiController@edit');
    Route::put('pegawai/update/{id}', 'PegawaiController@update');
    Route::delete('pegawai/{id}', 'PegawaiController@destroy');

    Route::get('cuti/ajukan-cuti/', 'CutiController@ajukan');    
    Route::post('cuti/store', 'CutiController@store');
    Route::get('cuti/proses-cuti/{id}', 'CutiController@proses');
    Route::post('cuti/update/{id}', 'CutiController@update');
    Route::delete('cuti/{id}', 'CutiController@destroy');

    Route::get('izin/ajukan-izin/', 'IzinController@ajukan');        
    Route::post('izin/store', 'IzinController@store');
    Route::get('izin/proses-izin/{id}', 'IzinController@proses');
    Route::post('izin/update/{id}', 'IzinController@update');
    Route::delete('izin/{id}', 'IzinController@destroy');

Route::get('/home', 'HomeController@index');
Auth::routes();

Route::get('/home', 'HomeController@index');
