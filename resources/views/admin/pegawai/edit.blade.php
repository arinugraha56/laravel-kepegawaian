@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                    <div class="panel-heading">Edit Data Pegawai</div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" action="{{ URL('pegawai/update/'.$showById->id) }}" method="POST">
                            {{ csrf_field() }}
                        <input type="hidden" name="_method" value="PUT"> <br/>
                        <div class="form-group{{ $errors->has('no_pegawai') ? ' has-error' : '' }}">
                            <label for="no_pegawai" class="col-md-4 control-label">Nomor Pegawai</label>
                            <div class="col-md-6">
                                <input id="no_pegawai" type="text" class="form-control" name="no_pegawai" value="{{ $showById->no_pegawai }}" autofocus>
                                @if ($errors->has('no_pegawai'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('no_pegawai') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Nama Pegawai</label>
                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ $showById->name }}" autofocus>
                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('jabatan') ? ' has-error' : '' }}">
                            <label for="jabatan" class="col-md-4 control-label">Jabatan</label>
                            <div class="col-md-6">
                                <input id="jabatan" type="text" class="form-control" name="jabatan" value="{{ $showById->jabatan }}" autofocus>
                                @if ($errors->has('jabatan'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('jabatan') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('jk_pegawai') ? ' has-error' : '' }}">
                            <label for="jk_pegawai" class="col-md-4 control-label">Jenis Kelamin</label>
                            <div class="col-md-6">
                                <label><input id="jk_pegawai" type="radio" name="jk_pegawai" value="Laki-laki" {{ $showById->jk_pegawai == 'Laki-laki' ? 'checked' : '' }} /> Laki-laki</label>
                                <label><input id="jk_pegawai" type="radio" name="jk_pegawai" value="Perempuan" {{ $showById->jk_pegawai == 'Perempuan' ? 'checked' : '' }} /> Perempuan</label>
                                @if ($errors->has('jk_pegawai'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('jk_pegawai') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        @php
                        $tgl_carbon = new \Carbon\Carbon($showById->tgl_lahir);
                        $tgl = $tgl_carbon->format('Y-m-d');
                        @endphp
                        <div class="form-group">
                                <label for="tgl_lahir" class="col-md-4 control-label">Tanggal Lahir</label>
                                <div class="col-md-6">
                                        <input type="date" id="tgl_lahir" name="tgl_lahir" value="{{ $tgl }}">
                                </div>
                                @if ($errors->has('tgl_lahir'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('tgl_lahir') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('status_pegawai') ? ' has-error' : '' }}">
                            <label for="status_pegawai" class="col-md-4 control-label">Status Pegawai</label>
                            <div class="col-md-6">
                                <input id="status_pegawai" type="text" class="form-control" name="status_pegawai" value="{{ $showById->status_pegawai }}" autofocus>
                                @if ($errors->has('status_pegawai'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('status_pegawai') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('agama') ? ' has-error' : '' }}">
                            <label for="agama" class="col-md-4 control-label">Agama</label>
                            <div class="col-md-6">
                                    <select name="agama">
                                            <option value=""></option>
                                            <option value="islam" {{ $showById->agama == 'islam' ? 'selected' : '' }}>Islam</option>
                                            <option value="kristen" {{ $showById->agama == 'kristen' ? 'selected' : '' }}>Kristen</option>
                                            <option value="hindu" {{ $showById->agama == 'hindu' ? 'selected' : '' }}>Hindu</option>
                                            <option value="budha" {{ $showById->agama == 'budha' ? 'selected' : '' }}>Budha</option>
                                        </select>
                                @if ($errors->has('agama'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('agama') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                <div class="form-group{{ $errors->has('tempat_lahir') ? ' has-error' : '' }}">
                    <label for="tempat_lahir" class="col-md-4 control-label">Tempat Lahir</label>
                    <div class="col-md-6">
                        <input id="tempat_lahir" type="text" class="form-control" name="tempat_lahir" value="{{ $showById->tempat_lahir }}" autofocus>
                        @if ($errors->has('tempat_lahir'))
                            <span class="help-block">
                                <strong>{{ $errors->first('tempat_lahir') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-group{{ $errors->has('alamat') ? ' has-error' : '' }}">
                    <label for="alamat" class="col-md-4 control-label">Alamat</label>
                    <div class="col-md-6">
                        <textarea id="alamat" class="form-control" name="alamat">{{ $showById->alamat }}</textarea>
                        @if ($errors->has('alamat'))
                            <span class="help-block">
                                <strong>{{ $errors->first('alamat') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail</label>
                            <div class="col-md-6">
                                <input id="email" type="text" class="form-control" name="email" value="{{ $showById->email }}">
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('no_hp') ? ' has-error' : '' }}">
                            <label for="no_hp" class="col-md-4 control-label">Nomor Telepon</label>
                            <div class="col-md-6">
                                <input id="no_hp" type="text" class="form-control" name="no_hp" value="{{ $showById->no_hp }}">
                                @if ($errors->has('no_hp'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('no_hp') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" name="submit" class="btn btn-primary" value="PUT">
                                    Ubah
                                </button>
                            </div>
                        </div>
                    </form>
                    <a href="{{ URL('home') }}"><button class="btn btn-default">Kembali</button></a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection