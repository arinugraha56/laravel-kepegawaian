@extends('layouts.app')

@section('content')
<style type="text/css">
   .i {
   color: #a19d91;
   font-style: italic;
   }
</style>
<div class="container">
    <div class="row">
        <div class="col-lg-8 col-md-offset-2">
             <div class="panel panel-default">
                <div class="panel-body">
                        <h2 class="page-header">Detail Karyawan</h2>
                        <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Detail Pegawai</h3> </div>
                                    <div class="pull-right">
                    </div> 
        <div class="col-lg-19">
                <!-- /.panel-heading -->
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                            <tbody>
            <tr>                                 
            <td class="col-lg-3">Nama</td>
            @if ($detail->name!=null)
            <td>{{ $detail->name }}</td>
            @else
            <td class="i">Tidak Ada Data</td>
            @endif
            </tr>

            <tr>
            <td>Nomor Pegawai</td>
            @if ($detail->no_pegawai!=null)
            <td>{{ $detail->no_pegawai }}</td>
            @else
            <td class="i">Tidak Ada Data</td>
            @endif
            </tr>

            <tr>
            <td>Jabatan</td>
            @if ($detail->jabatan!=null)
            <td>{{ $detail->jabatan }}</td>
            @else
            <td class="i">Tidak Ada Data</td>
            @endif
            </tr>

            <tr>
            <td>Jenis Kelamin</td>
            @if ($detail->jk_pegawai!=null)
            <td>{{ $detail->jk_pegawai }}</td>
            @else
            <td class="i">Tidak Ada Data</td>
            @endif
            </tr>
            
            @php
                $tgl_carbon = new \Carbon\Carbon($detail->tgl_lahir);
                $tgl = $tgl_carbon->format('Y-m-d');
            @endphp

            <tr>
            <td>Tanggal Lahir</td>
            @if ($detail->tgl_lahir!=null)
            <td>{{ $detail->tgl_lahir }}</td>
            @else
            <td class="i">Tidak Ada Data</td>
            @endif
            </tr>

            <tr>
            <td>Agama</td>
            @if ($detail->agama!=null)
            <td>{{ $detail->agama }}</td>
            @else
            <td class="i">Tidak Ada Data</td>            
            @endif
            </tr>

            <tr>
            <td>Tempat Lahir</td>
            @if ($detail->tempat_lahir!=null)
            <td>{{ $detail->tempat_lahir }}</td>
            @else
            <td class="i">Tidak Ada Data</td>            
            @endif
            </tr>

            <tr>
            <td>Alamat</td>
            @if ($detail->alamat!=null)
            <td>{{ $detail->alamat }}</td>
            @else
            <td class="i">Tidak Ada Data</td>
            @endif
            </tr>

            <tr>
            <td>Email</td>
            @if ($detail->email!=null)
            <td>{{ $detail->email }}</td>
            @else
            <td class="i">Tidak Ada Data</td>
            @endif
            </tr>

            <tr>
            <td>Nomor Telepon</td>
            @if ($detail->no_hp!=null)
            <td>{{ $detail->no_hp }}</td>
            @else
            <td class="i">Tidak Ada Data</td>
            @endif
            </tr>             
                            </tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
            <p><a href="{{ URL('home') }}"><button class="btn btn-default">Kembali</button></a> <a href="{{ URL('pegawai/edit') }}/{{ $detail->id }}"><button class="btn btn-default">Edit</button></a></p>
        </div><br>
        <!-- /.col-lg-6 -->
        </div>            
    </div>
</div>
@endsection