@extends('layouts.app')

@section('content')
@if (Session::get('success'))

        <div class="alert alert-success alert-block">
        
            <button type="button" class="close" data-dismiss="alert">×</button>	
        
                oaoe
        
        </div>
        @endif
<div class="container">
<div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading"><h2>Tambah Tugas</h2></div>
                <div class="panel-body">
                          {{-- part alert --}}
                @if (Session::has('after_save'))
                <div class="row">
                <div class="col-md-12">
                <div class="alert alert-dismissible alert-{{ Session::get('after_save.alert') }}">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{ Session::get('after_save.title') }}</strong>
                <a href="javascript:void(0)" class="alert-link">{{ Session::get('after_save.text-1') }}</a> {{ Session::get('after_save.text-2') }}
                </div>
            </div>
        </div>
        @endif
        {{-- end part alert --}}
    <form class="form-horizontal" role="form" method="POST" action="{{ URL('tugas/store') }}">
        {{ csrf_field() }}
        {{ method_field('post') }}
        <div class="form-group{{ $errors->has('no_pegawai') ? ' has-error' : '' }}">
                <label for="no_pegawai" class="col-md-4 control-label">Nomor Pegawai</label>
                <div class="col-md-6">
                    <select name="no_pegawai"  class="form-control col-md-7 col-xs-12">
                        <option value="">----Pilih Nomor Pegawai---</option>
                        @foreach ($user as $k)
                          <option value="{{$k->no_pegawai}}">{{$k->no_pegawai}}</option>
                        @endforeach
                      </select >
                    @if ($errors->has('no_pegawai'))
                        <span class="help-block">
                            <strong>{{ $errors->first('no_pegawai') }}</strong>
                        </span>
                    @endif
                </div>
            </div> 	
        <div class="form-group{{ $errors->has('nama_pegawai') ? ' has-error' : '' }}">
                <label for="nama_pegawai" class="col-md-4 control-label">Nama Pegawai</label>
                <div class="col-md-6">
                    {{-- <input id="nama_pegawai" type="text" class="form-control" name="nama_pegawai" value="{{ old('nama_pegawai') }}" autofocus> --}}
                    <select name="nama_pegawai"  class="form-control col-md-7 col-xs-12">
                        <option value="">----Pilih Nama Pegawai---</option>
                        @foreach ($user as $k)
                          <option value="{{$k->name}}">{{$k->name}}</option>
                        @endforeach
                      </select >
                    @if ($errors->has('nama_pegawai'))
                        <span class="help-block">
                            <strong>{{ $errors->first('nama_pegawai') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="form-group{{ $errors->has('JenisTugas') ? ' has-error' : '' }}">
                    <label for="JenisTugas" class="col-md-4 control-label">Deskripsi Tugas</label>
                    <div class="col-md-6">
                        <input id="JenisTugas" type="text" class="form-control" name="JenisTugas" value="{{ old('JenisTugas') }}" autofocus>
                        @if ($errors->has('JenisTugas'))
                            <span class="help-block">
                                <strong>{{ $errors->first('JenisTugas') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-group{{ $errors->has('DeadlineTugas') ? ' has-error' : '' }}">
                        <label for="DeadlineTugas" class="col-md-4 control-label">Tanggal Tugas</label>
                        <div class="col-md-6">
                            <input id="DeadlineTugas" type="date" name="DeadlineTugas" autofocus>
                            @if ($errors->has('DeadlineTugas'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('DeadlineTugas') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
    <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
    <button type="submit" name="submit" class="btn btn-primary">Simpan</button>
            </div>
    </div>
    </form>
  </div>
</div>
</div>
</div>
<a href="{{ URL('home') }}"><button class="btn btn-default">Kembali</button></a>
</div>
@endsection