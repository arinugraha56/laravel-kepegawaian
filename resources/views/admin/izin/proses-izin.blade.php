@extends('layouts.app')

@section('content')
   <h2 class="text-center">Proses Izin</h2>
   <br>
   <div class="col-md-4"></div>
   <div class="container col-md-4">
      <form action="{{url('izin/update/'.$izin_models->id)}}" method="post">
      {{csrf_field()}}
      <input type="hidden" name="id" value="{{$izin_models->id}}">
    
         <h4>Nama Pegawai</h4><label>{{$izin_models->pegawai->name}}</label>         
         <h4>No Pegawai</h4><label>{{$izin_models->pegawai->no_pegawai}}</label>
         
         @php
         $tgl_carbon = new \Carbon\Carbon($izin_models->tgl_mulai_izin);
         $tgl_start = $tgl_carbon->format('d-m-Y');
         @endphp

         <h4>Mulai izin</h4><label>{{$tgl_start}}</label>

         @php
         $tgl_carbon = new \Carbon\Carbon($izin_models->tgl_selesai_izin);
         $tgl_finish = $tgl_carbon->format('d-m-Y');
         @endphp

         <h4>Selesai izin</h4><label>{{$tgl_finish}}</label>
         <h4>Alasan izin</h4><label>{{$izin_models->alasan_pegawai}}</label>
         <div class="form-group">
         {{-- <form action="{{url('admin/update/'.$izin_models->id)}}" method="post"> --}}
         <h4>Status</h4>
            <input type="radio" name="status_izin" value="Terima" checked> Terima
            <input type="radio" name="status_izin" value="Tolak"> Tolak<br>
         <button type="submit" value="" class="btn btn-primary center-block">Proses</button>
      </div>
      </form>
    <a href="{{ URL('home') }}"><button class="btn btn-default">Kembali</button></a>
   </div>
  
{{-- 
   <script type="text/javascript">
            $(function () {
                $('#datetimepicker4').datetimepicker();
            });
        </script> --}}
@endsection