@extends('layouts.app')
@section('content')
   <h2 class="text-center">Proses Cuti</h2>
   <br>
   <div class="col-md-4"></div>
   <div class="container col-md-4">
      <form action="{{url('cuti/update/'.$cuti_models->id)}}" method="post">
      {{csrf_field()}}
      <input type="hidden" name="id" value="{{$cuti_models->id}}">
      
         <h4>Nama Pegawai</h4><label>{{$cuti_models->pegawai->name}}</label>         
         <h4>No Pegawai</h4><label>{{$cuti_models->pegawai->no_pegawai}}</label>
         
         @php
         $tgl_carbon = new \Carbon\Carbon($cuti_models->StartCuti);
         $tgl_start = $tgl_carbon->format('d-m-Y');
         @endphp
         
         <h4>Mulai Cuti</h4><label>{{$tgl_start}}</label>

         @php
         $tgl_carbon = new \Carbon\Carbon($cuti_models->FinishCuti);
         $tgl_finish = $tgl_carbon->format('d-m-Y');
         @endphp
         <h4>Finish Cuti</h4><label>{{$tgl_finish}}</label>

         <h4>Alasan Cuti</h4><label>{{$cuti_models->AlasanCuti}}</label>
         <div class="form-group">
         <form action="{{url('admin/update/'.$cuti_models->id)}}" method="post">
         <h4>Status</h4>
            <input type="radio" name="StatusCuti" value="Terima" checked> Terima
            <input type="radio" name="StatusCuti" value="Tolak"> Tolak<br>
         <button type="submit" value="" class="btn btn-primary center-block">Proses</button>
      </div>
      </form>
    <a href="{{ URL('home') }}"><button class="btn btn-default">Kembali</button></a>
   </div>
@endsection