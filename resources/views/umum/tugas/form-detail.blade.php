@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading"><h2>Detail Tugas</h2></div>
                <div class="panel-body">
    <form class="form-horizontal" role="form" action="{{ URL('tugas/selesai/'.$tugas_models->id) }}" method="POST">
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="PUT"> <br/>
        <div class="form-group">
            <label for="no_pegawai" class="col-md-4 control-label">No Pegawai</label>
            <div class="col-md-6">
          <input type="text" class="form-control" id="no_pegawai" name="no_pegawai" readonly value="{{ $tugas_models->no_pegawai }}" autofocus> 
      </div>
    </div>
    <div class="form-group">
            <label for="nama_pegawai" class="col-md-4 control-label">Nama Pegawai</label>
            <div class="col-md-6">
          <input type="text" class="form-control" id="nama_pegawai" name="nama_pegawai" readonly value="{{ $tugas_models->nama_pegawai }}" autofocus> 
      </div>
    </div>
    <div class="form-group">
            <label for="JenisTugas" class="col-md-4 control-label">Deskripsi Tugas</label>
            <div class="col-md-6">
          <input type="text" class="form-control" id="JenisTugas" name="JenisTugas" readonly value="{{ $tugas_models->JenisTugas }}" autofocus> 
      </div>
    </div>
    <div class="form-group">
            <label for="DeadlineTugas" class="col-md-4 control-label">Deadline Tugas</label>
            <div class="col-md-6"> 
                    <input type="date" id="DeadlineTugas" name="DeadlineTugas" readonly value="{{ $tugas_models->DeadlineTugas }}" autofocus>
            </div>
    </div>
    <div class="form-group">
        <label for="TanggalSelesai" class="col-md-4 control-label">Tanggal Selesai</label>
        <div class="col-md-6"> 
                <input type="date" id="TanggalSelesai" name="TanggalSelesai" autofocus>
        </div>
    </div>
    <div class="form-group{{ $errors->has('keterangan') ? ' has-error' : '' }}">
        <label for="keterangan" class="col-md-4 control-label">Status</label>
        <div class="col-md-6">
            <label><input id="keterangan" type="radio" name="keterangan" value="Terima" checked/> Terima</label>
            <label><input id="keterangan" type="radio" name="keterangan" value="Tolak" /> Tolak</label>
        </div>
    </div>
    <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
                <button type="submit" name="submit" class="btn btn-primary" value="PUT">Selesai</button>
            </div>
    </div>
    </form>
    <a href="{{ URL('home') }}"><button class="btn btn-default">Kembali</button></a>
  </div>
</div>
</div>
</div>
</div>
@endsection