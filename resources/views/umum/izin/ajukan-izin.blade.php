@extends('layouts.app')

@section('content')
<div class="container">
        <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="panel panel-default">
                        <div class="panel-heading"><h2>Form Izin</h2></div>
                        <div class="panel-body">
                                {{-- part alert --}}
                        @if (Session::has('after_save'))
                        <div class="row">
                        <div class="col-md-12">
                        <div class="alert alert-dismissible alert-{{ Session::get('after_save.alert') }}">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ Session::get('after_save.title') }}</strong>
                        <a href="javascript:void(0)" class="alert-link">{{ Session::get('after_save.text-1') }}</a> {{ Session::get('after_save.text-2') }}
                        </div>
                    </div>
                </div>
                @endif
                {{-- end part alert --}}
            <form class="form-horizontal" role="form" method="POST" action="{{ URL('izin/store') }}">
                {{ csrf_field() }}
                {{ method_field('post') }}
                <div class="form-group{{ $errors->has('tgl_mulai_izin') ? ' has-error' : '' }}">
                    <label for="tgl_mulai_izin" class="col-md-4 control-label">Mulai Izin</label>
                    <div class="col-md-6">
                        <input id="tgl_mulai_izin" type="date" name="tgl_mulai_izin" autofocus>
                        @if ($errors->has('tgl_mulai_izin'))
                            <span class="help-block">
                                <strong>{{ $errors->first('tgl_mulai_izin') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-group{{ $errors->has('tgl_selesai_izin') ? ' has-error' : '' }}">
                    <label for="tgl_selesai_izin" class="col-md-4 control-label">Selesai Izin</label>
                    <div class="col-md-6">
                        <input id="tgl_selesai_izin" type="date" name="tgl_selesai_izin" autofocus>
                        @if ($errors->has('tgl_selesai_izin'))
                            <span class="help-block">
                                <strong>{{ $errors->first('tgl_selesai_izin') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-group{{ $errors->has('alasan_pegawai') ? ' has-error' : '' }}">
                    <label for="alasan_pegawai" class="col-md-4 control-label">Alasan Izin</label>
                    <div class="col-md-6">
                        <input id="alasan_pegawai" type="text" class="form-control" name="alasan_pegawai" value="{{ old('alasan_pegawai') }}" autofocus>
                        @if ($errors->has('alasan_pegawai'))
                            <span class="help-block">
                                <strong>{{ $errors->first('alasan_pegawai') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
            <button type="submit" name="submit" class="btn btn-primary">Simpan</button>
                    </div>
            </div>
            </form>
        <a href="{{ URL('home') }}"><button class="btn btn-default">Kembali</button></a>
        </div>
        </div>
        </div>
        </div>
</div>
@endsection