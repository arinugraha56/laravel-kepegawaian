@extends('layouts.app')

@section('content')
<div class="container">
<div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading"><h2>Form Cuti</h2></div>
                <div class="panel-body">
                        {{-- part alert --}}
                @if (Session::has('after_save'))
                <div class="row">
                <div class="col-md-12">
                <div class="alert alert-dismissible alert-{{ Session::get('after_save.alert') }}">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{ Session::get('after_save.title') }}</strong>
                <a href="javascript:void(0)" class="alert-link">{{ Session::get('after_save.text-1') }}</a> {{ Session::get('after_save.text-2') }}
                </div>
            </div>
        </div>
        @endif
        {{-- end part alert --}}
    <form class="form-horizontal" role="form" method="POST" action="{{ URL('cuti/store') }}">
        {{ csrf_field() }}
        {{ method_field('post') }}
        <div class="form-group{{ $errors->has('StartCuti') ? ' has-error' : '' }}">
            <label for="StartCuti" class="col-md-4 control-label">Mulai Cuti</label>
            <div class="col-md-6">
                <input id="StartCuti" type="date" name="StartCuti" autofocus>
                @if ($errors->has('StartCuti'))
                    <span class="help-block">
                        <strong>{{ $errors->first('StartCuti') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="form-group{{ $errors->has('FinishCuti') ? ' has-error' : '' }}">
                <label for="FinishCuti" class="col-md-4 control-label">Selesai Cuti</label>
                <div class="col-md-6">
                    <input id="FinishCuti" type="date" name="FinishCuti" autofocus>
                    @if ($errors->has('FinishCuti'))
                        <span class="help-block">
                            <strong>{{ $errors->first('FinishCuti') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="form-group{{ $errors->has('AlasanCuti') ? ' has-error' : '' }}">
                    <label for="AlasanCuti" class="col-md-4 control-label">Alasan Cuti</label>
                    <div class="col-md-6">
                        <input id="AlasanCuti" type="text" class="form-control" name="AlasanCuti" value="{{ old('AlasanCuti') }}" autofocus>
                        @if ($errors->has('AlasanCuti'))
                            <span class="help-block">
                                <strong>{{ $errors->first('AlasanCuti') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
        <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
    <button type="submit" name="submit" class="btn btn-primary">Simpan</button>
            </div>
    </div>
    </form>
    <a href="{{ URL('home') }}"><button class="btn btn-default">Kembali</button></a>
    </div>
    </div>
    </div>
    </div>
</div>
    @endsection