@extends('layouts.app')

@section('content')
<style type="text/css">
    .i {
    color: #a19d91;
    font-style: italic;
    }
 </style>
<div class="container">

        
    <div class="row">
         
        <ul class="nav nav-tabs">
				
            @can('isAdmin')
                <li><a data-toggle="tab" href="#pegawai">DATA PEGAWAI</a></li>
                <li><a data-toggle="tab" href="#tugas">TUGAS</a></li>
                <li><a data-toggle="tab" href="#izin">IZIN</a></li>
                <li><a data-toggle="tab" href="#cuti">CUTI</a></li>
            @endcan
            
            @can('isMember')
            <div class="row">
                <div class="col-12">
                    <div class="card text-center">
                    <h3>Selamat Datang, {{ Auth::User()->name }}</h3>
                        <div class="card-body">
                            <div class="card-title mb-4">
                                <div class="d-flex justify-content-start">
                                    </div>
                                    {{-- <div class="userData ml-3">
                                        <h1 class="d-block" style="font-size: 2rem; font-weight: bold"><a href="javascript:void(0);">Timeline saat ini</a></h1>
                                  
                                        <div class="row">
                                                <div class="col-sm-4">
                                                  <div class="card">
                                                    <div class="card-body">
                                                    <h3 class="card-title" style="font-weight: bold">2</h3>
                                                      <p class="card-text">Tugas</p>
                                                    </div>
                                                  </div>
                                                </div>
                                                
                                                <div class="col-sm-4">
                                                  <div class="card">
                                                    <div class="card-body">
                                                      <h3 class="card-title" style="font-weight: bold">2</h3>
                                                      <p class="card-text">Ijin</p>
                                                    </div>
                                                  </div>
                                                </div>
    
                                                <div class="col-sm-4">
                                                  <div class="card">
                                                    <div class="card-body">
                                                    <h3 class="card-title" style="font-weight: bold">2</h3>
                                                    <p class="card-text">Cuti</p>
                                                    </div>
                                                  </div>
                                                </div>
                                          </div>
                                    </div> --}}
                                </div>
                            </div>
    
                            <div class="row">
                                <div class="col-12">
                                    <ul class="nav nav-tabs mb-4" id="myTab" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" id="basicInfo-tab" data-toggle="tab" href="#basicInfo" role="tab" aria-controls="basicInfo" aria-selected="true">Biodata</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="connectedServices-tab" data-toggle="tab" href="#p_cuti" role="tab" aria-controls="cuti" aria-selected="false">Cuti</a>
                                        </li>
    
                                        <li class="nav-item">
                                            <a class="nav-link" id="connectedServices-tab" data-toggle="tab" href="#p_izin" role="tab" aria-controls="ijin" aria-selected="false">Ijin</a>
                                        </li>
    
                                        <li class="nav-item">
                                            <a class="nav-link" id="connectedServices-tab" data-toggle="tab" href="#p_tugas" role="tab" aria-controls="tugas" aria-selected="false">Tugas</a>
                                        </li>
                                    </ul>
                                    <div class="tab-content clearfix">  
                                              
                                            </div>
    
                                        </div>
                                    </div>
                                </div>
                            </div>
    
    
                        </div>
                {{-- <li><a data-toggle="tab" href="#p_pegawai">PROFIL PEGAWAI</a></li>
                <li><a data-toggle="tab" href="#p_tugas">TUGAS</a></li>
                <li><a data-toggle="tab" href="#p_izin">IZIN</a></li>
                <li><a data-toggle="tab" href="#p_cuti">CUTI</a></li> --}}
            @endcan
		</ul>
          
    <!-- MASUKAN DATA TABEL SESUAI ARAHAN-->

    <div class="tab-content">
			
            
            <!--DISINI BAGIAN TABEL BUAT ADMIN -->
                        <div id="pegawai" class="tab-pane fade">
                                <br>
                                <div class="row">
                                    <div class="col-md-10 col-md-offset-1">
                                        <div class="panel panel-default">
                                            <div class="panel-body">
                                            <h2 class="text-center">Kepegawaian</h2>
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h3 class="panel-title">Data Pegawai</h3>
                                                    <div class="pull-right">
                                                    </div>
                                                </div>
                                                <div class="panel-body">
                                                    <input type="text" class="form-control" id="dev-table-filter" data-action="filter" data-filters="#dev-table" placeholder="Cari Pegawai" />
                                                </div>
                                                <table class="table table-hover" id="dev-table">
                                                    <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th>Nomor Pegawai</th> 
                                                            <th>Nama Pegawai</th> 
                                                            <th>Jabatan</th> 
                                                            <th>Email</th> 
                                                            <th>Nomor Telepon</th>
                                                            <th>Aksi</th>                                
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                            @php(
                                                                $no = 1 {{-- buat nomor urut --}}
                                                                )
                                                            {{-- loop all kendaraan --}}
                                                            @foreach ($user as $ambil_pegawai)
                                                            <tr>
                                                                <td>{{ $no++ }}</td>
                                                                @if ($ambil_pegawai->no_pegawai!=null)
                                                                <td>{{ $ambil_pegawai->no_pegawai }}</td>
                                                                @else
                                                                    <td class="i">Tidak Ada Data</td>
                                                                @endif
                                                                @if ($ambil_pegawai->name!=null)
                                                                <td>{{ $ambil_pegawai->name }}</td>
                                                                @else
                                                                     <td class="i">Tidak Ada Data</td>
                                                                @endif
                                                                @if ($ambil_pegawai->jabatan!=null)
                                                                <td>{{ $ambil_pegawai->jabatan }}</td>
                                                                @else
                                                                    <td class="i">Tidak Ada Data</td>
                                                                @endif
                                                                @if ($ambil_pegawai->email!=null)
                                                                <td>{{ $ambil_pegawai->email }}</td>
                                                                @else
                                                                    <td class="i">Tidak Ada Data</td>
                                                                @endif
                                                                @if ($ambil_pegawai->no_hp!=null)
                                                                <td>{{ $ambil_pegawai->no_hp }}</td>
                                                                @else
                                                                    <td class="i">Tidak Ada Data</td>
                                                                @endif
                                                            <td align="center" width="30%">
                                                                <a href="{{ URL('pegawai/show') }}/{{ $ambil_pegawai->id }}" class="btn btn-info btn-sm"><i class="fa fa-info fa-fw"></i> Detail</a>
                                                                <a href="{{ URL('pegawai/edit') }}/{{ $ambil_pegawai->id }}" class="btn btn-warning btn-sm"><i class="fa fa-pencil fa-fw"></i> Edit</a>
                                                                
                                                                {!! Form::open(array('url'=>array('pegawai', $ambil_pegawai->id),
                                                                '_token' => '{{ crsf_token() }}','style' => 'display:inline')) !!}
                                                                <input type="hidden" name="_method" value="GET">
                                                                 <button class='btn btn-sm btn-danger' type='submit'> <i class="fa fa-trash-o fa-fw" aria-hidden="true"></i> Hapus</button>
                                                            {!! Form::close() !!}                       
                                                            </td>
                                                        </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{-- <a href="{{ URL('') }}" class="btn btn-raised btn-danger pull-left"><i class="fa fa-plus" aria-hidden="true"></i> Tambah Data</a>  --}}
                    </div>
    
                    <div id="tugas" class="tab-pane fade">
                        <br>
                        
                        @if (empty($tugas_all))
                        <div class="row">
                            <div class="col-md-8 col-md-offset-2">
                                <div class="panel panel-default">
                                    <div class="panel-heading"><h2>Tambah Tugas</h2></div>
                                    <div class="panel-body">
                                              {{-- part alert --}}
                                    @if (Session::has('after_save'))
                                    <div class="row">
                                    <div class="col-md-12">
                                    <div class="alert alert-dismissible alert-{{ Session::get('after_save.alert') }}">
                                    <button type="button" class="close" data-dismiss="alert">×</button>
                                    <strong>{{ Session::get('after_save.title') }}</strong>
                                    <a href="javascript:void(0)" class="alert-link">{{ Session::get('after_save.text-1') }}</a> {{ Session::get('after_save.text-2') }}
                                    </div>
                                </div>
                            </div>
                            @endif
                            {{-- end part alert --}}
                        <form class="form-horizontal" role="form" method="POST" action="{{ URL('tugas/store') }}">
                            {{ csrf_field() }}
                            {{ method_field('post') }}
                            <div class="form-group{{ $errors->has('no_pegawai') ? ' has-error' : '' }}">
                                    <label for="no_pegawai" class="col-md-4 control-label">Nomor Pegawai</label>
                                    <div class="col-md-6">
                                        <select name="no_pegawai"  class="form-control col-md-7 col-xs-12">
                                            <option value="">----Pilih Nomor Pegawai---</option>
                                            @foreach ($user as $k)
                                              <option value="{{$k->no_pegawai}}">{{$k->no_pegawai}}</option>
                                            @endforeach
                                          </select >
                                        @if ($errors->has('no_pegawai'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('no_pegawai') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div> 	
                            <div class="form-group{{ $errors->has('nama_pegawai') ? ' has-error' : '' }}">
                                    <label for="nama_pegawai" class="col-md-4 control-label">Nama Pegawai</label>
                                    <div class="col-md-6">
                                        {{-- <input id="nama_pegawai" type="text" class="form-control" name="nama_pegawai" value="{{ old('nama_pegawai') }}" autofocus> --}}
                                        <select name="nama_pegawai"  class="form-control col-md-7 col-xs-12">
                                            <option value="">----Pilih Nama Pegawai---</option>
                                            @foreach ($user as $k)
                                              <option value="{{$k->name}}">{{$k->name}}</option>
                                            @endforeach
                                          </select >
                                        @if ($errors->has('nama_pegawai'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('nama_pegawai') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('JenisTugas') ? ' has-error' : '' }}">
                                        <label for="JenisTugas" class="col-md-4 control-label">Deskripsi Tugas</label>
                                        <div class="col-md-6">
                                            <input id="JenisTugas" type="text" class="form-control" name="JenisTugas" value="{{ old('JenisTugas') }}" autofocus>
                                            @if ($errors->has('JenisTugas'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('JenisTugas') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group{{ $errors->has('DeadlineTugas') ? ' has-error' : '' }}">
                                            <label for="DeadlineTugas" class="col-md-4 control-label">Tanggal Tugas</label>
                                            <div class="col-md-6">
                                                <input id="DeadlineTugas" type="date" name="DeadlineTugas" autofocus>
                                                @if ($errors->has('DeadlineTugas'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('DeadlineTugas') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                        <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                        <button type="submit" name="submit" class="btn btn-primary">Simpan</button>
                                </div>
                        </div>
                        </form>
                      </div>
                    </div>
                    </div>
                    </div>
                        @else
                        <div class="row">
                            <div class="col-md-10 col-md-offset-1">
                                <div class="panel panel-default">
                    
                                    <div class="panel-body">
                                    <h2 class="text-center">INFO TUGAS</h2>
                                    <div class="panel panel-primary">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">Data Tugas</h3>
                                            <div class="pull-right">
                                            </div>
                                        </div>
                                        <div class="panel-body">
                                            <input type="text" class="form-control" id="dev-table-filter" data-action="filter" data-filters="#dev-table" placeholder="Cari Tugas" />
                                        </div>
                                        <table class="table table-hover" id="dev-table">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>No Pegawai</th>
                                                    <th>Nama Pegawai</th>
                                                    <th>Deskripsi Tugas</th>
                                                    <th>Deadline Tugas</th>
                                                    <th>Tanggal Selesai</th>                                  
                                                    <th>Keterangan</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($tugas_all as $no => $ambil_tugas)
                                                    <tr>
                                                        <td>{{ $no+1 }}</td>
                                                        <td>{{ $ambil_tugas->no_pegawai }}</td>                                                            
                                                        <td>{{ $ambil_tugas->nama_pegawai }}</td>
                                                        <td>{{ $ambil_tugas->JenisTugas }}</td>
                                                        <td>{{ $ambil_tugas->DeadlineTugas }}</td>
                                                        @if($ambil_tugas->TanggalSelesai==null)
                                                        <td class="i">Tugas Belum Selesai</td>
                                                        <td>{{ $ambil_tugas->keterangan }}</td>
                                                        @else
                                                        <td>{{ $ambil_tugas->TanggalSelesai }}</td>
                                                        <td><a href="" class="btn btn-primary btn-sm active" role="button" aria-pressed="true">Telah Selesai</a></td>
                                                        @endif
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                    </div>
                                </div>
                            </div>
                            <a href="{{ URL('tugas/tambah') }}" class="btn btn-raised btn-danger pull-left"><i class="fa fa-plus" aria-hidden="true"></i> Tambah Tugas</a> 
                        </div>
                            @endif
                           
                    </div>                
                            <div id="izin" class="tab-pane fade">
                                <br>    
                                <div class="row">
                                            <div class="col-md-10 col-md-offset-1">
                                                <div class="panel panel-default">
                                    
                                                    <div class="panel-body">
                                                    <h2 class="text-center">Kepegawaian</h2>
                                                    <div class="panel panel-primary">
                                                        <div class="panel-heading">
                                                            <h3 class="panel-title">Data Izin Pegawai</h3>
                                                            <div class="pull-right">
                                                            </div>
                                                        </div>
                                                        <div class="panel-body">
                                                            <input type="text" class="form-control" id="dev-table-filter" data-action="filter" data-filters="#dev-table" placeholder="Cari Pegawai" />
                                                        </div>
                                                        <table class="table table-hover" id="dev-table">
                                                            <thead>
                                                                    @foreach ($izin_all as $pgw)
                                                                        <tr>
                                                                                <th>#</th>
                                                                                <th>Nomor Pegawai</th>
                                                                                <th>Nama Pegawai</th>
                                                                                <th>Mulai Izin</th>
                                                                                <th>Selesai Izin</th>
                                                                                <th>Alasan Izin</th>
                                                                                @if($pgw->status_izin=='Belum Diproses')
                                                                                <th>Status Izin</th>
                                                                                <th>Keterangan</th>                                                                                
                                                                                @else
                                                                                <th>Status Izin</th>
                                                                                <th>Keterangan</th>                                                                                
                                                                                @endif
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                                    @php(
                                                                                        $no = 1 {{-- buat nomor urut --}}
                                                                                        )
                                                                            <tr> 
                                                                                <td>{{$no++}}</td>
                                                                                <td>{{$pgw->pegawai->no_pegawai}}</td>
                                                                                <td>{{$pgw->pegawai->name}}</td>

                                                                                @php
                                                                                $tgl_carbon = new \Carbon\Carbon($pgw->tgl_mulai_izin);
                                                                                $tgl_start = $tgl_carbon->format('d-m-Y');
                                                                                @endphp
                                                                                
                                                                                <td>{{$tgl_start}}</td>
                                                                                
                                                                                @php
                                                                                $tgl_carbon = new \Carbon\Carbon($pgw->tgl_selesai_izin);
                                                                                $tgl_finish = $tgl_carbon->format('d-m-Y');
                                                                                @endphp

                                                                                <td>{{$pgw->tgl_selesai_izin}}</td>
                                                                                
                                                                                <td>{{$pgw->alasan_pegawai}}</td>
                                                                                @if($pgw->status_izin=='Belum Diproses')                                                                                
                                                                                <td>{{$pgw->status_izin}}</td>                                                                                
                                                                                <td><a href="{{url('izin/proses-izin/'.$pgw->id)}}" class="btn btn-primary btn-sm active" role="button" aria-pressed="true">Proses</a></td>
                                                                                @else
                                                                                <td>{{$pgw->status_izin}}</td>                                                                                
                                                                                <td><a href="" class="btn btn-primary btn-sm active" role="button" aria-pressed="true">Telah Diproses</a></td>
                                                                                @endif
                                                                        </tr>
                                                                    @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                            </div>
                            <div id="cuti" class="tab-pane fade">
                                    <br>    
                                    <div class="row">
                                                <div class="col-md-10 col-md-offset-1">
                                                    <div class="panel panel-default">
                                        
                                                        <div class="panel-body">
                                                        <h2 class="text-center">Kepegawaian</h2>
                                                        <div class="panel panel-primary">
                                                            <div class="panel-heading">
                                                                <h3 class="panel-title">Data Cuti Pegawai</h3>
                                                                <div class="pull-right">
                                                                </div>
                                                            </div>
                                                            <div class="panel-body">
                                                                <input type="text" class="form-control" id="dev-table-filter" data-action="filter" data-filters="#dev-table" placeholder="Cari Pegawai" />
                                                            </div>
                                                            <table class="table table-hover" id="dev-table">
                                                                <thead>
                                                                    @foreach ($cuti_all as $tampil)
                                                                    <tr>
                                                                        <th>#</th>
                                                                        <th>Nomor Pegawai</th> 
                                                                        <th>Nama Pegawai</th> 
                                                                        <th>Mulai Cuti</th> 
                                                                        <th>Selesai Cuti</th>
                                                                        <th>Alasan Cuti</th>
                                                                        @if($tampil->StatusCuti=='Belum Diproses')
                                                                        <th>Status Cuti</th>
                                                                        <th>Keterangan</th>                                                                                
                                                                        @else
                                                                        <th>Status Cuti</th>
                                                                        <th>Keterangan</th>                                                                                
                                                                        @endif
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                        @php(
                                                                            $i = 1 {{-- buat nomor urut --}}
                                                                            )
                                                                    <tr>
                                                                        <td>{{$i++}}</td>
                                                                        <td>{{$tampil->pegawai->no_pegawai}}</td>
                                                                        <td>{{$tampil->pegawai->name}}</td>
                                                                        
                                                                        @php
                                                                        $tgl_carbon = new \Carbon\Carbon($tampil->StartCuti);
                                                                        $tgl_start = $tgl_carbon->format('d-m-Y');
                                                                        @endphp
                                                                        
                                                                        <td>{{$tgl_start}}</td>
                                                                        
                                                                        @php
                                                                        $tgl_carbon = new \Carbon\Carbon($tampil->FinishCuti);
                                                                        $tgl_finish = $tgl_carbon->format('d-m-Y');
                                                                        @endphp

                                                                        <td>{{$tgl_finish}}</td>

                                                                        <td>{{$tampil->AlasanCuti}}</td>
                                                                        @if($tampil->StatusCuti=='Belum Diproses')
                                                                        <td>{{$tampil->StatusCuti}}</td>
                                                                        <td><a href="{{url('cuti/proses-cuti/'.$tampil->id)}}" class="btn btn-primary btn-sm active" role="button" aria-pressed="true">Proses</a></td>
                                                                        @else
                                                                        <td>{{$tampil->StatusCuti}}</td>
                                                                        <td><a href="" class="btn btn-primary btn-sm active" role="button" aria-pressed="true">Telah Diproses</a></td>                                                                        
                                                                        @endif
                                                                    </tr>
                                                                    @endforeach
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        </div>
                                                    </div>
                                                </div>
                                          </div>
                                </div>
        <!--MASUKAN DISINI UNTUK DATA TAB MEMBER/USER -->
        <div class="tab-pane fade" id="basicInfo">
            <hr />
            <div class="row">
                <div class="col-sm-3 col-md-6 col-6">
                    <label style="font-weight:bold;">Nomor Pegawai</label>
                    <p>{{ Auth::User()->no_pegawai }}</p>
                </div>
                <div class="col-sm-3 col-md-6 col-6">
                    <label style="font-weight:bold;">Alamat</label>
                    <p>{{ Auth::User()->alamat }}</p>                    
                </div>
            </div>
            <div class="row">
            <div class="col-sm-3 col-md-6 col-6">
                    <label style="font-weight:bold;">Nama Pegawai</label>
                    <p>{{ Auth::User()->name }}</p>
                </div>
                <div class="col-sm-3 col-md-6 col-6">
                    <label style="font-weight:bold;">E-mail</label>
                    <p>{{ Auth::User()->email }}</p>
                </div>
            </div>
            <div class="row">
            <div class="col-sm-3 col-md-6 col-6">
                    <label style="font-weight:bold;">Jabatan</label>
                    <p>{{ Auth::User()->jabatan }}</p>
                </div>
                <div class="col-sm-3 col-md-6 col-6">
                    <label style="font-weight:bold;">Telp</label>
                    <p>{{ Auth::User()->no_hp }}</p>
                </div>
            </div>
            <hr />
        </div>
                    <div id="p_tugas" class="tab-pane fade">
                    <br>
                    <div class="row">
                            <div class="col-md-10 col-md-offset-1">
                                <div class="panel panel-default">
                    
                                    <div class="panel-body">
                                    <h2 class="text-center">INFO TUGAS</h2>
                                    <div class="panel panel-primary">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">Tugas</h3>
                                            <div class="pull-right">
                                            </div>
                                        </div>
                                        <div class="panel-body">
                                            <input type="text" class="form-control" id="dev-table-filter" data-action="filter" data-filters="#dev-table" placeholder="Cari Tugas" />
                                        </div>
                                        <table class="table table-hover" id="dev-table">
                                            <thead>
                                                @foreach ($tugas as $tampil_tugas)
                                                <tr>
                                                    <th>#</th>
                                                    <th>Ditunjukkan ke</th>
                                                    <th>Deskripsi Tugas</th>								
                                                    <th>Deadline Tugas</th>
                                                    @if ($tampil_tugas->TanggalSelesai==null)                                  
                                                    <th>Tanggal Selesai</th>                                                    
                                                    <th>Aksi</th>
                                                    @else
                                                    <th>Tanggal Selesai</th>
                                                    <th>Aksi</th>
                                                    @endif
                                                </tr>
                                            </thead>
                                            <tbody>
                                                    @php(
                                                        $no = 1 {{-- buat nomor urut --}}
                                                        )
                                                    {{-- loop all kendaraan --}}
                                                    <tr>
                                                        <td>{{ $no++ }}</td>
                                                        <td>{{ $tampil_tugas->nama_pegawai }}</td>
                                                        <td>{{ $tampil_tugas->JenisTugas }}</td>
                                                        <td>{{ $tampil_tugas->DeadlineTugas }}</td>
                                                        @if ($tampil_tugas->TanggalSelesai==null)                                  
                                                        <td class="i">Tidak Ada Data</td>                                                        
                                                        <td align="center" width="30%">
                                                        <a href="{{ URL('tugas/proses-tugas') }}/{{ $tampil_tugas->id }}" class="btn btn-warning btn-sm"><i class="fa fa-pencil fa-fw"></i>Selesai</a></td>
                                                        @else
                                                        <td>{{ $tampil_tugas->TanggalSelesai }}</td>
                                                        <td>{{ $tampil_tugas->keterangan }}</td>
                                                        @endif
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>      

            <div id="p_izin" class="tab-pane fade">
                    <br>
                    @foreach ($izin as $tampil_izin)
                    @if ($tampil_izin->id!=null)
                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                <a href="{{ URL('izin/ajukan-izin/') }}" class="btn btn-raised btn-danger pull-left"><i class="fa fa-plus" aria-hidden="true"></i> Ajukan Izin</a>                                 
                                <h2 class="text-center">Kepegawaian</h2>
                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Data Izin Pegawai</h3>
                                        <div class="pull-right">
                                        </div>
                                    </div>
                                    <div class="panel-body">
                                        <input type="text" class="form-control" id="dev-table-filter" data-action="filter" data-filters="#dev-table" placeholder="Cari Pegawai" />
                                    </div>
                                    <table class="table table-hover" id="dev-table">
                                        <thead>
                                                    <tr>
                                                            <th>#</th>
                                                            <th>Nomor Pegawai</th>
                                                            <th>Nama Pegawai</th>
                                                            <th>Mulai Izin</th>
                                                            <th>Selesai Izin</th>
                                                            <th>Alasan Izin</th>
                                                            <th>Status Izin</th>                                                                                
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                                @php(
                                                                    $no = 1 {{-- buat nomor urut --}}
                                                                    )
                                                        <tr> 
                                                            <td>{{$no++}}</td>
                                                            <td>{{$pgw->pegawai->no_pegawai}}</td>
                                                            <td>{{$pgw->pegawai->name}}</td>

                                                            @php
                                                            $tgl_carbon = new \Carbon\Carbon($tampil_izin->tgl_mulai_izin);
                                                            $tgl_start = $tgl_carbon->format('d-m-Y');
                                                            @endphp
                                                            
                                                            <td>{{$tgl_start}}</td>
                                                            
                                                            @php
                                                            $tgl_carbon = new \Carbon\Carbon($tampil_izin->tgl_selesai_izin);
                                                            $tgl_finish = $tgl_carbon->format('d-m-Y');
                                                            @endphp

                                                            <td>{{$tgl_finish}}</td>
                                                            <td>{{$pgw->alasan_pegawai}}</td>
                                                            <td>{{$pgw->status_izin}}</td>
                                                    </tr>
                                        </tbody>
                                    </table>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @else
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <div class="panel panel-default">
                                <div class="panel-heading"><h2>Form Izin</h2></div>
                                <div class="panel-body">
                                        {{-- part alert --}}
                                @if (Session::has('after_save'))
                                <div class="row">
                                <div class="col-md-12">
                                <div class="alert alert-dismissible alert-{{ Session::get('after_save.alert') }}">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>{{ Session::get('after_save.title') }}</strong>
                                <a href="javascript:void(0)" class="alert-link">{{ Session::get('after_save.text-1') }}</a> {{ Session::get('after_save.text-2') }}
                                </div>
                            </div>
                        </div>
                        @endif
                        {{-- end part alert --}}
                    <form class="form-horizontal" role="form" method="POST" action="{{ URL('izin/store') }}">
                        {{ csrf_field() }}
                        {{ method_field('post') }}
                        <div class="form-group{{ $errors->has('tgl_mulai_izin') ? ' has-error' : '' }}">
                            <label for="tgl_mulai_izin" class="col-md-4 control-label">Mulai Izin</label>
                            <div class="col-md-6">
                                <input id="tgl_mulai_izin" type="date" name="tgl_mulai_izin" autofocus>
                                @if ($errors->has('tgl_mulai_izin'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('tgl_mulai_izin') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('tgl_selesai_izin') ? ' has-error' : '' }}">
                            <label for="tgl_selesai_izin" class="col-md-4 control-label">Selesai Izin</label>
                            <div class="col-md-6">
                                <input id="tgl_selesai_izin" type="date" name="tgl_selesai_izin" autofocus>
                                @if ($errors->has('tgl_selesai_izin'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('tgl_selesai_izin') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('alasan_pegawai') ? ' has-error' : '' }}">
                            <label for="alasan_pegawai" class="col-md-4 control-label">Alasan Izin</label>
                            <div class="col-md-6">
                                <input id="alasan_pegawai" type="text" class="form-control" name="alasan_pegawai" value="{{ old('alasan_pegawai') }}" autofocus>
                                @if ($errors->has('alasan_pegawai'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('alasan_pegawai') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                    <button type="submit" name="submit" class="btn btn-primary">Simpan</button>
                            </div>
                    </div>
                    </form>
                </div>
                </div>
                </div>
                </div>
                @endif
                @endforeach
        </div>

            <div id="p_cuti" class="tab-pane fade">
                    <br>
                    @foreach ($cuti as $tampil_cuti)
                    @if ($tampil_cuti->id!=null)
                    <div class="row">
                            <div class="col-md-10 col-md-offset-1">
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                    <a href="{{ URL('cuti/ajukan-cuti/') }}" class="btn btn-raised btn-danger pull-left"><i class="fa fa-plus" aria-hidden="true"></i> Ajukan Cuti</a>                                    
                                    <h2 class="text-center">Kepegawaian</h2>
                                    <div class="panel panel-primary">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">Data Cuti Pegawai</h3>
                                            <div class="pull-right">
                                            </div>
                                        </div>
                                        <div class="panel-body">
                                            <input type="text" class="form-control" id="dev-table-filter" data-action="filter" data-filters="#dev-table" placeholder="Cari Pegawai" />
                                        </div>
                                        <table class="table table-hover" id="dev-table">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Nomor Pegawai</th> 
                                                    <th>Nama Pegawai</th> 
                                                    <th>Mulai Cuti</th> 
                                                    <th>Selesai Cuti</th>
                                                    <th>Alasan Cuti</th>
                                                    <th>Status Cuti</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                    @php(
                                                        $i = 1 {{-- buat nomor urut --}}
                                                        )
                                                <tr>
                                                    <td>{{$i++}}</td>
                                                    <td>{{$tampil->pegawai->no_pegawai}}</td>
                                                    <td>{{$tampil->pegawai->name}}</td>
                                                    
                                                    @php
                                                    $tgl_carbon = new \Carbon\Carbon($tampil_cuti->StartCuti);
                                                    $tgl_start = $tgl_carbon->format('d-m-Y');
                                                    @endphp
                                                    
                                                    <td>{{$tgl_start}}</td>
                                                    
                                                    @php
                                                    $tgl_carbon = new \Carbon\Carbon($tampil_cuti->FinishCuti);
                                                    $tgl_finish = $tgl_carbon->format('d-m-Y');
                                                    @endphp

                                                    <td>{{$tgl_finish}}</td>
                                                    <td>{{$tampil->AlasanCuti}}</td>
                                                    <td>{{$tampil->StatusCuti}}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    </div>
                                </div>
                            </div>
                      </div>
                    @else
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <div class="panel panel-default">
                                <div class="panel-heading"><h2>Form Cuti</h2></div>
                                <div class="panel-body">
                                        {{-- part alert --}}
                                @if (Session::has('after_save'))
                                <div class="row">
                                <div class="col-md-12">
                                <div class="alert alert-dismissible alert-{{ Session::get('after_save.alert') }}">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>{{ Session::get('after_save.title') }}</strong>
                                <a href="javascript:void(0)" class="alert-link">{{ Session::get('after_save.text-1') }}</a> {{ Session::get('after_save.text-2') }}
                                </div>
                            </div>
                        </div>
                        @endif
                        {{-- end part alert --}}
                    <form class="form-horizontal" role="form" method="POST" action="{{ URL('cuti/store') }}">
                        {{ csrf_field() }}
                        {{ method_field('post') }}
                        <div class="form-group{{ $errors->has('StartCuti') ? ' has-error' : '' }}">
                            <label for="StartCuti" class="col-md-4 control-label">Mulai Cuti</label>
                            <div class="col-md-6">
                                <input id="StartCuti" type="date" name="StartCuti" autofocus>
                                @if ($errors->has('StartCuti'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('StartCuti') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('FinishCuti') ? ' has-error' : '' }}">
                                <label for="FinishCuti" class="col-md-4 control-label">Selesai Cuti</label>
                                <div class="col-md-6">
                                    <input id="FinishCuti" type="date" name="FinishCuti" autofocus>
                                    @if ($errors->has('FinishCuti'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('FinishCuti') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('AlasanCuti') ? ' has-error' : '' }}">
                                    <label for="AlasanCuti" class="col-md-4 control-label">Alasan Cuti</label>
                                    <div class="col-md-6">
                                        <input id="AlasanCuti" type="text" class="form-control" name="AlasanCuti" value="{{ old('AlasanCuti') }}" autofocus>
                                        @if ($errors->has('AlasanCuti'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('AlasanCuti') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                    <button type="submit" name="submit" class="btn btn-primary">Simpan</button>
                            </div>
                    </div>
                    </form>
                    </div>
                    </div>
                    </div>
                    </div>
                    @endif
                    @endforeach
            </div>
                </div>
         </div>
</div>
@endsection
