<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class izin extends Model
{
    protected $table = "izin";

    public $timestamps = true;

    protected $fillable = ['alasan_pegawai'];
    
    protected $dates = ['tgl_mulai_izin', 'tgl_selesai_izin'];

    public function pegawai()
     {
        return $this->belongsTo('App\User', 'pegawai_id');
    }
    
}
