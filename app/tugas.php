<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tugas extends Model
{
    protected $table = "tugas";

    public $timestamps = true;

    protected $fillable = ['no_pegawai', 'nama_pegawai', 'JenisTugas', 'DeadlineTugas'];
}
