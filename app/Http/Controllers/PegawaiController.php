<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class PegawaiController extends Controller
{

    public function show($id)
    {
        $detail = User::find($id);
        
        return view('admin.pegawai.detail', compact('detail'));
    }

    public function edit($id)
    {
        $showById = User::find($id);
        
        return view('admin.pegawai.edit', compact('showById'));
    }

    public function update(Request $request, $id)
    {

         $validate = \Validator::make($request->all(), [
            'no_pegawai' => 'required',
            'name' => 'required',
            'jabatan' => 'required', 
            'jk_pegawai' => 'required',
            'tgl_lahir' => 'required',
            'status_pegawai' => 'required',
            'agama' => 'required',
            'tempat_lahir' => 'required',
            'alamat' => 'required',
            'email' => 'required',
            'no_hp' => 'required'
          ]);

        $after_update = [
            'alert' => 'danger',
            'title' => 'Oh wait!',
            'text-1' => 'Ada kesalahan',
            'text-2' => 'Silakan coba lagi.'
        ];

    if($validate->fails()){
        return redirect()->back()->with('after_update', $after_update);
    }

    $after_update = [
        'alert' => 'success',
        'title' => 'Good Job!',
        'text-1' => 'data berhasil diubah',
    ];

        $ubah = User::find($id);
        
        $ubah->no_pegawai = $request->no_pegawai;
        $ubah->name = $request->name;
        $ubah->jabatan = $request->jabatan;
        $ubah->jk_pegawai = $request->jk_pegawai;
        $ubah->tgl_lahir = $request->tgl_lahir;
        $ubah->status_pegawai = $request->status_pegawai;
        $ubah->agama = $request->agama;
        $ubah->tempat_lahir = $request->tempat_lahir;
        $ubah->alamat = $request->alamat;
        $ubah->email = $request->email;
        $ubah->no_hp = $request->no_hp;

        $ubah->update();
        return redirect()->to('/home')->with('after_update', $after_update);
    }

    public function destroy($id)
    {
    $hapus = User::find($id);
    $hapus->delete();
    return redirect()->back();
    }
}

    // public function create()
    // {
    //     $input = new pegawai();

    //     return view('admin.pegawai.create', compact('input'));
    // }

    // public function index(){
    //     return view('pegawai-beranda', compact('pegawai'));
    // }

    // public function store(Request $request)
    // {

    //     $validate = \Validator::make($request->all(), [
    //         'no_pegawai' => 'required',
    //         'nama_pegawai' => 'required',
    //         'jabatan' => 'required', 
    //         'jk_pegawai' => 'required',
    //         'golongan' => 'required',
    //         'tgl_lahir' => 'required',
    //         'status_pegawai' => 'required',
    //         'agama' => 'required',
    //         'tgl_masuk' => 'required',
    //         'tgl_keluar' => 'required',
    //         'tempat_lahir' => 'required',
    //         'status_pernikahan' => 'required',
    //         'alamat' => 'required',
    //         'email' => 'required',
    //         'no_hp' => 'required',
    //         'nama_pekerjaan' => 'required'
    //       ]);

    //     $after_save = [
    //         'alert' => 'danger',
    //         'title' => 'Oh wait!',
    //         'text-1' => 'Ada kesalahan',
    //         'text-2' => 'Silakan coba lagi.'
    //     ];

    // if($validate->fails()){
    //     return redirect()->back()->with('after_save', $after_save);
    // }

    // $after_save = [
    //     'alert' => 'success',
    //     'title' => 'Good Job!',
    //     'text-1' => 'Tambah lagi',
    //     'text-2' => 'Atau kembali.'
    // ];

    
    // $input = new pegawai();

    //     $input->no_pegawai = $request->no_pegawai;  
    //     $input->nama_pegawai = $request->nama_pegawai;
    //     $input->jabatan = $request->jabatan;
    //     $input->jk_pegawai = $request->jk_pegawai;
    //     $input->golongan = $request->golongan;
    //     $input->tgl_lahir = $request->tgl_lahir;
    //     $input->status_pegawai = $request->status_pegawai;
    //     $input->agama = $request->agama;
    //     $input->tgl_masuk = $request->tgl_masuk;
    //     $input->tgl_keluar = $request->tgl_keluar;
    //     $input->tempat_lahir = $request->tempat_lahir;
    //     $input->status_pernikahan = $request->status_pernikahan;
    //     $input->alamat = $request->alamat;
    //     $input->email = $request->email;
    //     $input->nama_pekerjaan = $request->nama_pekerjaan;
    //     $input->no_hp = $request->no_hp;

    //     // dd($input);
    //     $input->save();
    //         return redirect()->to('/home');
    // }
