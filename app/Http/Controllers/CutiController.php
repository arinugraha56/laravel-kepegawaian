<?php

namespace App\Http\Controllers;
use Auth;
use App\cuti;
use App\User;
use Illuminate\Http\Request;

class CutiController extends Controller
{
    public function ajukan(){
        
        $tampil = new cuti();

        return view('umum.cuti.ajukan-cuti', compact('tampil'));        
    }
    public function store(Request $request)
    {   
        $validate = \Validator::make($request->all(), [
            'StartCuti' => 'required',
            'FinishCuti' => 'required',
            'AlasanCuti' => 'required'
          ]);

        $after_save = [
            'alert' => 'danger',
            'title' => 'Oh wait!',
            'text-1' => 'Ada kesalahan',
            'text-2' => 'Silakan coba lagi.'
        ];

    if($validate->fails()){
        return redirect()->back()->with('after_save', $after_save);
    }

    $after_save = [
        'alert' => 'success',
        'title' => 'Good Job!',
        'text-1' => 'Tambah lagi',
        'text-2' => 'Atau kembali.'
    ];

       $input= new cuti();

       $input->StartCuti=$request->StartCuti;
       $input->FinishCuti=$request->FinishCuti;
       $input->StatusCuti='Belum Diproses';
       $input->AlasanCuti=$request->AlasanCuti;
       $input->pegawai_id=Auth::User()->id;

       $input->save();
       return redirect()->back()->with('after_save', $after_save);
       }

       public function proses($id){
        $cuti_models=cuti::find($id);
       return view('admin.cuti.proses-cuti', compact('cuti_models'));
        }


    public function update(Request $request, $id){
        $update=cuti::find($id);
        $update->StatusCuti = $request->StatusCuti;
        $update->admin_id=Auth::User()->id;
        $update->update();
            return redirect('/home');
    }
}
