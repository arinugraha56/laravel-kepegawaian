<?php

namespace App\Http\Controllers;

use Auth;
use App\Providers\SweetAlertServiceProvider;
use App\tugas;
use App\cuti;
use App\izin;
use App\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $users = User::all();
        $tugas_all = tugas::all();        
        $tugas = tugas::where('no_pegawai', Auth::user()->no_pegawai)->get();
        $izin = izin::where('pegawai_id', Auth::user()->id)->get();
        $cuti = cuti::where('pegawai_id', Auth::user()->id)->get();
        $cuti_all = cuti::all();
        $izin_all = izin::all();

        return view('home', ['user'=>$users],  compact('cuti', 'izin', 'tugas', 'tugas_all', 'cuti_all', 'izin_all'));
    }
}
