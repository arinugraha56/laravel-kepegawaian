<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use App\tugas;
use App\User;

class TugasController extends Controller
{

    public function create()
    {
        $users = User::all();
        $input = new tugas();

        return view('admin.tugas.tambah-tugas', ['user'=>$users], compact('input'));
    }

    public function store(Request $request)
    {   
        $validate = \Validator::make($request->all(), [
            'no_pegawai' => 'required',            
            'nama_pegawai' => 'required',
            'JenisTugas' => 'required',
            'DeadlineTugas' => 'required'          
          ]);

        $after_save = [
            'alert' => 'danger',
            'title' => 'Oh wait!',
            'text-1' => 'Ada kesalahan',
            'text-2' => 'Silakan coba lagi.'
        ];

    if($validate->fails()){
        return redirect()->back()->with('after_save', $after_save);
    }

    $after_save = [
        'alert' => 'success',
        'title' => 'Good Job!',
        'text-1' => 'Tambah lagi',
        'text-2' => 'Atau kembali.'
    ];
       $input= new tugas();
// queri untuk ngmbil id user

       $input->no_pegawai=$request->no_pegawai;
       $input->nama_pegawai=$request->nama_pegawai;
       $input->JenisTugas=$request->JenisTugas;
       $input->DeadlineTugas=$request->DeadlineTugas;
       $input->keterangan='Belum Selesai';
       $input->admin_id=Auth::User()->id;

       $input->save();
        return redirect()->back()->with('success', 'success bro');
    }

    public function proses($id){
        $tugas_models=tugas::find($id);
       return view('umum.tugas.form-detail', compact('tugas_models'));
    }

    public function update(Request $request, $id){
        $update=tugas::find($id);
        $update->TanggalSelesai = $request->TanggalSelesai;
        $update->keterangan = $request->keterangan;
        $update->update();
            return redirect('/home');
    }
}
