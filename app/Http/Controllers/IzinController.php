<?php

namespace App\Http\Controllers;
use Auth;
use App\User;
use App\izin;
use Illuminate\Http\Request;

class IzinController extends Controller
{
    public function ajukan(){
        
        $tampil = new izin();

        return view('umum.izin.ajukan-izin', compact('tampil'));        
    }

    public function store(Request $request)
    {
        $validate = \Validator::make($request->all(), [
            'tgl_mulai_izin' => 'required',
            'tgl_selesai_izin' => 'required',
            'alasan_pegawai' => 'required'      
          ]);

        $after_save = [
            'alert' => 'danger',
            'title' => 'Oh wait!',
            'text-1' => 'Ada kesalahan',
            'text-2' => 'Silakan coba lagi.'
        ];

    if($validate->fails()){
        return redirect()->back()->with('after_save', $after_save);
    }

    $after_save = [
        'alert' => 'success',
        'title' => 'Good Job!',
        'text-1' => 'Tambah lagi',
        'text-2' => 'Atau kembali.'
    ];

        $input = new izin();

        $input->tgl_mulai_izin=$request->tgl_mulai_izin;
        $input->tgl_selesai_izin=$request->tgl_selesai_izin;
        $input->status_izin='Belum Diproses';
        $input->alasan_pegawai=$request->alasan_pegawai;
        $input->pegawai_id=Auth::User()->id;
        $input->save();
            return redirect()->back()->with('after_save', $after_save);
    }
    
    public function proses($id){
        $izin_models=izin::find($id);
       return view('admin.izin.proses-izin', compact('izin_models'));
    }


    public function update(Request $request, $id){
        $update=izin::find($id);
        $update->status_izin = $request->status_izin;
        $update->admin_id=Auth::User()->id;
        $update->update();
            return redirect('/home');
    }
}
