<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class cuti extends Model
{
    protected $table = 'cuti';

    public $timestamps = true;
    
    protected $fillable = ['AlasanCuti'];
    
    protected $dates = ['StartCuti', 'FinishCuti'];

     //relasi many to one (Saya adalah anggota dari model ......)
     public function pegawai()
     {
        return $this->belongsTo('App\User', 'pegawai_id');
    }
}