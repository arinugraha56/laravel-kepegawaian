<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'username', 'email', 'password', 'no_pegawai', 'jabatan', 'jk_pegawai', 'tgl_lahir', 'status_pegawai', 'agama', 'tempat_lahir', 'alamat', 'no_hp',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $dates = ['tgl_lahir'];

    //relasi many to one (Saya adalah anggota dari model ......)
    public function Cuti()
    {
       return $this->hasOne('App\cuti');
   }

   public function Izin()
    {
       return $this->hasOne('App\izin');
   }
}
