<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // config(['app.locale' => 'id']);
        // \Carbon\Carbon::setLocale('id');echo \Carbon\Carbon::now()->format('m-d-Y H:i:s');
        // // Sabtu, 04 Maret 2017 07:38
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
